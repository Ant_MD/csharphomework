﻿using System;

namespace Homework.Bisection
{
    public class BisectionSolver
    {
        public static double Solve(Func<double, double> f, double minX, double maxX, double error = double.Epsilon)
        {
            if (Math.Sign(f(minX)) == Math.Sign(f(maxX)))
            {
                return double.NaN;
            }

            var center = (maxX - minX) / 2 + minX;
            var isIncreasing = f(maxX) > 0;

            while (Math.Abs(f(center)) >= error)
            {
                center = (maxX - minX) / 2 + minX;

                if (f(center) < 0 ^ isIncreasing)
                {
                    maxX = center;
                }
                else
                {
                    minX = center;
                }
            }

            return center;
        }
    }
}
