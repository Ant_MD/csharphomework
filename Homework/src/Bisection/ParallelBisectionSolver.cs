using System;
using System.Collections.Generic;

namespace Homework.Bisection
{
    public class ParallelBisectionSolver
    {
        private readonly List<Func<double>> _fs = new List<Func<double>>();
 

        public void AddFunction(Func<double, double> f, double minX, double maxX, double error = double.Epsilon)
        {
            _fs.Add(() => BisectionSolver.Solve(f, minX, maxX, error)); 
        }

        public List<double> SolveAll()
        {
            var results = new List<double>(_fs.Count);
            var asyncResults = new List<IAsyncResult>(_fs.Count);

            foreach (var f in _fs)
            {
                asyncResults.Add(f.BeginInvoke(null, null));
            }

            for (var i = 0; i < _fs.Count; i++)
            {
                results.Add(_fs[i].EndInvoke(asyncResults[i]));
            }

            return results;
        }
    }
}