﻿using System;

namespace Homework
{
    public struct Complex
    {
        public double Real { get; }
        public double Imaginary { get; }

        public Complex(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        public Complex Add(Complex c)
        {
            return new Complex(Real + c.Real, Imaginary + c.Imaginary);
        }

        public Complex Add(params Complex[] cs)
        {
            var result = new Complex(0, 0);

            foreach (var c in cs)
            {
                result += c;
            }

            return result;
        }

        public Complex Sub(Complex c)
        {
            return new Complex(Real - c.Real, Imaginary - c.Imaginary);
        }

        public Complex Mult(Complex c)
        {
            var real = Real * c.Real - Imaginary * c.Imaginary;
            var imaginary = Imaginary * c.Real + Real * c.Imaginary;
            return new Complex(real, imaginary);
        }

        public Complex Mult(params Complex[] cs)
        {
            var result = new Complex(1, 1);

            foreach (var c in cs)
            {
                result *= c;
            }

            return result;
        }

        public Complex Div(Complex c)
        {
            var real = (Real * c.Real + Imaginary * c.Imaginary);
            var imaginary = Imaginary * c.Real - Real * c.Imaginary;
            var denom = Math.Pow(c.Real, 2) + Math.Pow(c.Imaginary, 2);
            return new Complex(real / denom, imaginary / denom);
        }

        public Complex Negative()
        {
            return new Complex(-Real, -Imaginary);
        }

        public double Abs()
        {
            return Math.Sqrt(Math.Pow(Real, 2) + Math.Pow(Imaginary, 2));
        }

        public override bool Equals(object o)
        {
            if (ReferenceEquals(null, o)) return false;
            return o is Complex && Equals((Complex) o);
        }

        public bool Equals(Complex other)
        {
            return Real.Equals(other.Real) && Imaginary.Equals(other.Imaginary);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Real.GetHashCode() * 397) ^ Imaginary.GetHashCode();
            }
        }

        public override string ToString()
        {
            return "Complex(" + Real + "," + Imaginary + ")";
        }

        public static bool operator ==(Complex a, Complex b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Complex a, Complex b)
        {
            return !a.Equals(b);
        }

        public static Complex operator +(Complex a, Complex b)
        {
            return a.Add(b);
        }

        public static Complex operator -(Complex a)
        {
            return a.Negative();
        }

        public static Complex operator -(Complex a, Complex b)
        {
            return a.Sub(b);
        }

        public static Complex operator *(Complex a, Complex b)
        {
            return a.Mult(b);
        }

        public static Complex operator /(Complex a, Complex b)
        {
            return a.Div(b);
        }

        public static explicit operator double(Complex c)
        {
            return c.Abs();
        }
    }
}
