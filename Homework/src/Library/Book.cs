﻿namespace Homework.Library
{
    public class Book
    {
        public string Title  { get; }
        public string Author { get; }
        public bool   IsRare { get; set; }
        public BookRentalInfo RentalInfo { get; set; }
        
        public Book(string title, string author, bool isRare)
        {
            Title = title;
            Author = author;
            IsRare = isRare;
        }
    }
}
