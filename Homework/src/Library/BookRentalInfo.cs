using System;

namespace Homework.Library
{
    public class BookRentalInfo
    {
        public Subscriber Renter { get; }
        public DateTime   ReceiptDate { get; }

        public BookRentalInfo(Subscriber renter, DateTime receiptDate)
        {
            Renter = renter;
            ReceiptDate = receiptDate;
        }
    }
}