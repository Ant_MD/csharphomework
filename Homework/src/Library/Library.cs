﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework.Library
{
    public class Library
    {
        public event Action<Book>            BookAdded         = delegate { };
        public event Action<Subscriber>      SubscriberAdded   = delegate { }; 
        public event Action<Book, BookState> BookStateChanged  = delegate { };  

        private readonly List<Subscriber> _subscribers = new List<Subscriber>();
        private readonly List<Book>       _books       = new List<Book>();

        public Book this[string author, string title]
        {
            get
            {
                return _books.Find(book => book.Author == author && book.Title == title);
            }
        }

        public void AddSubscriber(Subscriber subscriber)
        {
            _subscribers.Add(subscriber);

            SubscriberAdded(subscriber);
        }

        public List<Subscriber> GetSubscribers()
        {
            return _subscribers;
        }

        public void AddBook(Book book)
        {
            _books.Add(book);

            BookAdded(book);
        }

        public List<Book> GetBooksInLibrary()
        {
            return _books;
        }

        public List<Book> GetRentedBooks()
        {
            var rentedBooks = new List<Book>();

            foreach (var subscriber in _subscribers)
            {
                rentedBooks.AddRange(subscriber.GetRentedBooks());
            }

            return rentedBooks;
        }

        public List<Book> GetAllBooks()
        {
            var allBooks = new List<Book>();
            allBooks.AddRange(GetBooksInLibrary());
            allBooks.AddRange(GetRentedBooks());

            return allBooks;
        }

        public List<Book> FindBookskByTitle(string title)
        {
            return _books.FindAll(book => book.Title == title);
        }

        public List<Book> FindBooksByAuthor(string author)
        {
            return _books.FindAll(book => book.Author == author);
        }

        public void ReturnBook(Subscriber subscriber, Book book)
        {
            if(subscriber.RemoveBook(book))
            {
                book.RentalInfo = null;
                _books.Add(book);

                BookStateChanged(book, BookState.Returned);
            }
        }

        public bool GiveBook(Subscriber subscriber, Book book)
        {

            var haveOverdueBooks      = subscriber.GetOverdueBooks(DateTime.Now).Any();
            var haveLessThenFiveBooks = subscriber.GetRentedBooks().Count < 5;
            var haveRareBook          = subscriber.GetRentedBooks().Any(b => b.IsRare);
            
            if (!haveOverdueBooks && haveLessThenFiveBooks && !(haveRareBook && book.IsRare))
            {
                if (_books.Remove(book))
                {
                    book.RentalInfo = new BookRentalInfo(subscriber, DateTime.Now);

                    subscriber.AddBook(book);

                    BookStateChanged(book, BookState.Rented);

                    return true;
                }
            }

            return false;
        }
    }

    public enum BookState
    {
        Rented,
        Returned
    }
}
