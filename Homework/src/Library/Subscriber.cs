﻿using System;
using System.Collections.Generic;

namespace Homework.Library
{
    public class Subscriber
    {
        private readonly List<Book> _rentedBooks = new List<Book>();

        public string Name { get; set; }
        public string PhoneNumber { get; set; }

        public Subscriber(string name, string phoneNumber)
        {
            Name = name;
            PhoneNumber = phoneNumber;
        }

        public Book this[int i] => _rentedBooks[i];

        public List<Book> GetRentedBooks()
        {
            return _rentedBooks;
        }

        public List<Book> GetOverdueBooks(DateTime time)
        {
            var overdueBooks = new List<Book>();
            
            foreach (var book in _rentedBooks)
            {
                if ((time - book.RentalInfo.ReceiptDate).Days > 14)
                {
                    overdueBooks.Add(book);
                }
            }

            return overdueBooks;
        }

        public void AddBook(Book book)
        {
            _rentedBooks.Add(book);
        }

        public bool RemoveBook(Book book)
        {
            return _rentedBooks.Remove(book);
        }
    }
}
