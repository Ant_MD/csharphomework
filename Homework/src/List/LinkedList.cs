﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Homework.List
{
    public class LinkedList<T> : ICollection<T>
    {
        private LinkedListElement<T> _first;
        private LinkedListElement<T> _last;

        public event Action<T> Added   = delegate { };
        public event Action<T> Removed = delegate { };


        public T First => _first.Value;
        public T Last  => _last.Value;

        public bool IsReadOnly => false;

        public int Count { get; private set; }

        public LinkedList()
        {
            Count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var curr = _first; curr != null; curr = curr.Next)
            {
                yield return curr.Value;
            }
        }
        

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        

        public void Add(T item)
        {
            var newElem = new LinkedListElement<T>(item);

            if (Count == 0)
            {
                _first = newElem;
                _last = newElem;
            }
            else
            {
                _last.Next = newElem;

                _last = newElem;
            }

            Count++;

            Added(newElem.Value);
        }

        public void Clear()
        {
            for (var curr = _first; curr != null; curr = curr.Next)
            {
                curr.Previous = null;
                curr.Next     = null;

                Removed(curr.Value);
            }

            _first = null;
            _last  = null;

            Count = 0;
        }

        public bool Contains(T item)
        {
            for (var curr = _first; curr != null; curr = curr.Next)
            {
                if (item.Equals(curr.Value))
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (var curr = _first; curr != null; curr = curr.Next)
            {
                array[arrayIndex] = curr.Value;

                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            switch (Count)
            {
                case 0:
                    return false;
                case 1:
                    if (item.Equals(_first.Value))
                    {
                        Removed(_first.Value);

                        _first = null;
                        _last = null;

                        Count--;

                        return true;
                    }

                    return false;
                case 2:
                    if (item.Equals(_first.Value))
                    {
                        Removed(_first.Value);

                        _first = _last;

                        Count--;

                        return true;
                    }
                    if (item.Equals(_last.Value))
                    {
                        Removed(_last.Value);

                        _last = _first;

                        Count--;

                        return true;
                    }

                    return false;

            }

            for (var curr = _first; curr != null; curr = curr.Next)
            {
                if (item.Equals(curr.Value))
                {
                    Removed(curr.Value);

                    if (curr.Previous != null)
                    {
                        curr.Previous.Next = curr.Next;
                    }

                    if (curr.Next != null)
                    {
                        curr.Next.Previous = curr.Previous;
                    }
                    
                    curr.Previous = null;
                    curr.Next = null;

                    Count--;

                    return true;
                }
            }

            return false;
        }
    }
}
