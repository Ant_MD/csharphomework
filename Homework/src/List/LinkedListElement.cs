﻿namespace Homework.List
{
    internal class LinkedListElement<T>
    {
        public LinkedListElement<T> Next     { get; set; }
        public LinkedListElement<T> Previous { get; set; }

        public T Value { get; set; }

        public LinkedListElement(T value)
        {
            Value = value;
        }
    }
}
