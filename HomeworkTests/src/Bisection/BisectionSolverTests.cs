﻿using System;
using Homework.Bisection;
using NUnit.Framework;

namespace HomeworkTests.Bisection
{
    [TestFixture()]
    public class BisectionSolverTests
    {
        [Test()]
        public void SolveTest()
        { 
            const double error = 1E-10;
            Assert.LessOrEqual(Math.Abs(BisectionSolver.Solve(x => x,         1000, 1000, error)  -  0), error);
            Assert.LessOrEqual(Math.Abs(BisectionSolver.Solve(x => -x + 10,   1000, 1000, error)  - 10), error);
            Assert.LessOrEqual(Math.Abs(BisectionSolver.Solve(x => x * x - 9,    0,  100, error)  -  3), error);
        }
    }
}
