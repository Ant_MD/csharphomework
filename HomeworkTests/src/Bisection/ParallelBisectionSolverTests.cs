﻿using System.Collections.Generic;
using Homework.Bisection;
using NUnit.Framework;

namespace HomeworkTests.Bisection
{
    [TestFixture()]
    public class ParallelBisectionSolverTests
    {
        [Test()]
        public void AddFunctionTest()
        {
        }

        [Test()]
        public void SolveAllTest()
        {
            var pbs = new ParallelBisectionSolver();
            
            pbs.AddFunction(x =>            x, -1000, 1000);
            pbs.AddFunction(x =>       x + 10, -1000, 1000);
            pbs.AddFunction(x =>      x - 999, -1000, 1000);
            pbs.AddFunction(x => -x / 5 + 100, -1000, 1000);

            var results = pbs.SolveAll();


            Assert.AreEqual(  0, results[0]);
            Assert.AreEqual(-10, results[1]);
            Assert.AreEqual(999, results[2]);
            Assert.AreEqual(500, results[3]);
        }
    }
}