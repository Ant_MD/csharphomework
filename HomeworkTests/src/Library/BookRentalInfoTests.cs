﻿using System;
using Homework.Library;
using NUnit.Framework;

namespace HomeworkTests.Library
{
    [TestFixture()]
    public class BookRentalInfoTests
    {
        [Test()]
        public void BookRentalInfoTest()
        {
            var subscriber = new Subscriber("Anna", "000-0000");
            var currTime = DateTime.Now;

            var rentalInfo = new BookRentalInfo(subscriber, currTime);

            Assert.AreEqual(subscriber, rentalInfo.Renter);
            Assert.AreEqual(currTime, rentalInfo.ReceiptDate);
        }
    }
}