﻿using Homework.Library;
using NUnit.Framework;

namespace HomeworkTests.Library
{
    [TestFixture()]
    public class BookTests
    {
        [Test()]
        public void BookTest()
        {
            var book = new Book("Book1", "Author 1", true);

            Assert.AreEqual("Book1", book.Title);
            Assert.AreEqual("Author 1", book.Author);
            Assert.AreEqual(true, book.IsRare);
        }
    }
}