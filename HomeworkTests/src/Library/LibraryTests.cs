﻿using System.Collections.Generic;
using Homework.Library;
using NUnit.Framework;

namespace HomeworkTests.Library
{
    [TestFixture()]
    public class LibraryTests
    {
        private Homework.Library.Library _library;
        private Subscriber[] _subscribers;
        private Book[] _books;

        [SetUp()]
        public void SetUp()
        {
            _library = new Homework.Library.Library();

            _subscribers = new []{
                new Subscriber("Anna", "000-0000"),
                new Subscriber("Bob", "000-0001")
            };

            foreach (var subscriber in _subscribers)
            {
                _library.AddSubscriber(subscriber);
            }

            _books = new []{
                new Book("Book1", "Author1", false),
                new Book("Book2", "Author1", false),
                new Book("Book3", "Author2", false),
                new Book("Book4", "Author2", false),
                new Book("Book5", "Author2", false),
                new Book("Book6", "Author2", true),
                new Book("Book7", "Author3", true)
            };

            foreach (var book in _books)
            {
                _library.AddBook(book);
            }
        }

        [Test()]
        public void LibraryTest()
        {
        }

        [Test()]
        public void IndexerTest()
        {
            foreach(var book in _books)
            {
                Assert.AreEqual(book, _library[book.Author, book.Title]);
            }
        }

        [Test()]
        public void AddSubscriberTest()
        {
            var addedSubscribers = new List<Subscriber>();

            _library.SubscriberAdded += delegate(Subscriber subscriber)
            {
                addedSubscribers.Add(subscriber);
            };

            var newSubscriber = new Subscriber("Carl", "000-0002");

            _library.AddSubscriber(newSubscriber);

            Assert.Contains(newSubscriber, _library.GetSubscribers());
            Assert.Contains(newSubscriber, addedSubscribers);
        }

        [Test()]
        public void GetSubscribersTest()
        {
            foreach (var subscriber in _subscribers)
            {
                Assert.Contains(subscriber, _library.GetSubscribers());
            }
        }

        [Test()]
        public void AddBookTest()
        {
            var addedBooks = new List<Book>();

            _library.BookAdded += delegate (Book book)
            {
                addedBooks.Add(book);
            };

            var newBook = new Book("New Book", "New Author", false);

            _library.AddBook(newBook);

            Assert.Contains(newBook, _library.GetAllBooks());
            Assert.Contains(newBook, addedBooks);
        }

        [Test()]
        public void GetBooksInLibraryTest()
        {
            foreach (var book in _books)
            {
                Assert.Contains(book, _library.GetBooksInLibrary());
            }

            _library.GiveBook(_subscribers[0], _books[0]);

            Assert.IsFalse(_library.GetBooksInLibrary().Contains(_books[0]));
        }

        [Test()]
        public void GetRentedBooksTest()
        {
            _library.GiveBook(_subscribers[0], _books[0]);

            Assert.Contains(_books[0], _library.GetRentedBooks());
        }

        [Test()]
        public void GetAllBooksTest()
        {
            _library.GiveBook(_subscribers[0], _books[0]);
            _library.GiveBook(_subscribers[1], _books[1]);

            foreach (var book in _books)
            {
                Assert.Contains(book, _library.GetAllBooks());
            }
        }

        [Test()]
        public void FindBookByTitleTest()
        {
            foreach (var book in _books)
            {
                Assert.Contains(book, _library.FindBookskByTitle(book.Title));
            }
        }

        [Test()]
        public void FindBookByAuthorTest()
        {
            foreach (var book in _books)
            {
                Assert.Contains(book, _library.FindBooksByAuthor(book.Author));
            }
        }

        [Test()]
        public void ReturnBookTest()
        {
            var returnedBooks = new List<Book>();

            _library.BookStateChanged += delegate (Book book, BookState state)
            {
                if (state == BookState.Returned)
                {
                    returnedBooks.Add(book);
                }
            };

            _library.GiveBook(_subscribers[0], _books[0]);

            Assert.IsFalse(_library.GetBooksInLibrary().Contains(_books[0]));

            _library.ReturnBook(_subscribers[0], _books[0]);

            Assert.Contains(_books[0], _library.GetBooksInLibrary());
            Assert.Contains(_books[0], returnedBooks);

        }

        [Test()]
        public void GiveBookTest()
        {
            var rentedBooks = new List<Book>();

            _library.BookStateChanged += delegate (Book book, BookState state)
            {
                if (state == BookState.Rented)
                {
                    rentedBooks.Add(book);
                }
            };

            _library.GiveBook(_subscribers[0], _books[0]);

            Assert.Contains(_books[0], _library.GetRentedBooks());
            Assert.Contains(_books[0], _subscribers[0].GetRentedBooks());
            Assert.Contains(_books[0], rentedBooks);
        }

    }
}