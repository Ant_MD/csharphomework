﻿using System;
using Homework.Library;
using NUnit.Framework;

namespace HomeworkTests.Library
{
    [TestFixture()]
    public class SubscriberTests
    {
        private Subscriber _subscriber;
        private Book[]     _books;

        [SetUp()]
        public void SetUp()
        {
            _subscriber = new Subscriber("Anna", "000-0000");

            _books = new[]{
                new Book("Book1", "Author1", false),
                new Book("Book2", "Author2", false),
                new Book("Book3", "Author2", false),
                new Book("Book4", "Author3", true)
            };

            foreach(var book in _books)
            {
                book.RentalInfo = new BookRentalInfo(_subscriber, DateTime.Now);
                _subscriber.AddBook(book);
            }
        }

        [Test()]
        public void SubscriberTest()
        {
            var newSubscriber = new Subscriber("Subs Name", "000-0000");

            Assert.AreEqual("Subs Name", newSubscriber.Name);
            Assert.AreEqual("000-0000", newSubscriber.PhoneNumber);
        }

        [Test()]
        public void IndexerTest()
        {
            for(var i = 0; i < _books.Length; i++)
            {
                Assert.AreEqual(_books[i], _subscriber[i]);
            } 
        }

        [Test()]
        public void GetRentedBooksTest()
        {
            foreach(var book in _books)
            {
                Assert.Contains(book, _subscriber.GetRentedBooks());
            }
        }

        [Test()]
        public void GetOverdueBooksTest()
        {
            foreach (var book in _books)
            {
                Assert.IsFalse(_subscriber.GetOverdueBooks(DateTime.Now).Contains(book));
                Assert.Contains(book, _subscriber.GetOverdueBooks(DateTime.Now.AddDays(16)));
            }
        }

        [Test()]
        public void AddBookTest()
        {
            var newBook = new Book("Book5", "Author3", false);

            _subscriber.AddBook(newBook);

            Assert.Contains(newBook, _subscriber.GetRentedBooks());
        }

        [Test()]
        public void RemoveBookTest()
        {
            foreach(var book in _books)
            {
                _subscriber.RemoveBook(book);
                Assert.IsFalse(_subscriber.GetOverdueBooks(DateTime.Now).Contains(book));
            }
        }
    }
}