﻿using Homework.List;
using NUnit.Framework;

namespace HomeworkTests.List
{
    [TestFixture()]
    public class LinkedListTests
    {
        private LinkedList<int> _list;
        private int[] _elements;

        [SetUp()]
        public void SetUp()
        {
            _list = new LinkedList<int>();

            _elements = new []
            {
                1, 2, 3
            };

            foreach (var element in _elements)
            {
                _list.Add(element);
            }
        }

        [Test()]
        public void LinkedListTest()
        {
            var newLibrary = new LinkedList<int>();

            Assert.AreEqual(0, newLibrary.Count);
        }

        [Test()]
        public void GetEnumeratorTest()
        {
            foreach (var elem in _list)
            {
                Assert.Contains(elem, _elements);
            }
        }

        [Test()]
        public void AddTest()
        {
            _list.Add(4);

            Assert.AreEqual(4, _list.Last);
            Assert.AreEqual(_elements.Length + 1, _list.Count);
        }

        [Test()]
        public void ClearTest()
        {
            _list.Clear();

            Assert.AreEqual(0, _list.Count);
        }

        [Test()]
        public void ContainsTest()
        {
            foreach (var element in _elements)
            {
                Assert.True(_list.Contains(element));
            }
        }

        [Test()]
        public void CopyToTest()
        {
            var array = new int[_elements.Length + 1];

            _list.CopyTo(array, 1);
            
            for (var i = 0; i < _elements.Length; i++)
            {
                Assert.AreEqual(_elements[i], array[i + 1]);
            }
        }

        [Test()]
        public void RemoveTest()
        {
            Assert.False(_list.Remove(0));

            Assert.True(_list.Remove(1));

            Assert.AreEqual(_elements.Length - 1, _list.Count);
        }
    }
}